require File.expand_path('lib/midia')
require File.expand_path('lib/livro')
require File.expand_path('lib/cd')
require File.expand_path('lib/dvd')
require File.expand_path('lib/revista')
require File.expand_path('lib/biblioteca')
require File.expand_path('lib/relatorio')
require File.expand_path('lib/banco_de_arquivos')
require File.expand_path('lib/document_not_found')
require File.expand_path('lib/active_file')
require File.expand_path('lib/formatador_moeda')


crepusculo = Livro.new("Crepusculo", "Stephanie Meyer", "77562938-687", 389, 35.00, :vampiro)
game_of_thrones = Livro.new("GoT", "George R. R. Martin", "77777777-777", 700, 90.00, :morte)
biblia = Livro.new("Biblia", "Deus", "77777777-777", 1500, 800.00, :sagrado)
harry_potter_box = Livro.new("H.P.", "J. K. Rowling", "66492-646", 10984, 200.00, :bruxaria)

windows  = CD.new("Windows 95", 239.9, :SO)

os_x = DVD.new("MAC OS X", 98.9, :sistemas)

mundo_estranho = Revista.new("Mundo Estranho", 34.99)

biblioteca  = VendaFacil::Biblioteca.new

biblioteca.adiciona(crepusculo)
biblioteca.adiciona(game_of_thrones)
biblioteca.adiciona(biblia)
biblioteca.adiciona(harry_potter_box)
biblioteca.adiciona windows
biblioteca.adiciona(mundo_estranho)

biblioteca.each do |midia|
  p midia.titulo
end

puts os_x.preco_com_desconto_formatado
puts windows.preco_com_desconto_formatado
puts crepusculo.preco_com_desconto_formatado


mundo_estranho.save
mundo_estranho.preco = 99.00
mundo_estranho.save
begin 
  mundo_estranho = Revista.find 1
rescue DocumentNotFound
  p "The object wasn't found bitch!"
end
p mundo_estranho.preco
mundo_estranho.destroy
mundo_estranho.destroy
p DVD.ancestors[4].methods.select { |metodo| metodo == :puts}