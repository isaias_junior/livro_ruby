require File.expand_path('lib/formatador_moeda')

class Midia
  attr_accessor :preco, :titulo

  def initialize
    @desconto = 0.1
    @preco = 10.0
  end

  #def preco_com_desconto
    #@preco - (@preco * @desconto)
  #end

  def preco_com_desconto
    @preco - desconto
  end

  private

  def desconto 
    @preco * @desconto
  end


end
