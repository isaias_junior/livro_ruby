# encoding: utf-8
module VendaFacil

  class Biblioteca

    include Enumerable
  
    attr_accessor :livros

    def initialize
      @midias = []
      @banco_de_arq = BancoDeArquivos.new
    end

    def adiciona(midia)
      salva(midia) do
        midias << midia
      end
    end

    def midias
      @midias ||= @banco_de_arq.carrega
    end

    def midias_por_categoria(categoria)
      midias.select do |midia| 
        midia.categoria == categoria if midia.respond_to?(:categoria)
      end
    end

    def each
      midias.each {|midia| yield midia }
    end

    private

    def salva(midia)
      @banco_de_arq.salva(midia)
      yield
    end

  end

end