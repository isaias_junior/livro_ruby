require "FileUtils"

class Revista
  attr_reader :titulo, :id, :destroyed, :new_record
  attr_accessor :preco

  def initialize(titulo, preco)
    @titulo = titulo
    @preco = preco
    @id = self.class.next_id
    @destroyed = false
    @new_record = true
  end

  def save
    File.open("db/revistas/#{@id}.yml", "w") do |file|
      file.puts serialize
    end

    @new_record = false
  end

  def destroy
    unless @destroyed or @new_record
      @destroyed = true
      FileUtils.rm "db/revistas/#{@id}.yml"
    end
  end

  def self.find(id)
    raise DocumentNotFound, 
      "Document db/revistas/#{id} not found MOtherfucker!", 
      caller unless File.exists?("db/revistas/#{id}.yml") 
    YAML.load File.open("db/revistas/#{id}.yml", "r")
  end

  private

  def serialize
    YAML.dump self
  end

  def self.next_id
    Dir.glob("db/revistas/*.yml").size + 1
  end
end
