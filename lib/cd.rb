class CD < Midia
  
  include FormatadorMoeda

  FormatadorMoeda::formata_moeda :preco, :preco_com_desconto

  def initialize(titulo, preco, categoria)
    super()
    @titulo = titulo
    @preco = preco
    @categoria = categoria
  end

  def to_s 
    %Q{ Titulo: #{@titulo}, Preco: #{@preco} }
  end

end